import pathlib
from torch_geometric.transforms import (
    Compose,
    FixedPoints,
)

dataset_path = pathlib.Path(__file__).parent.resolve() / 'dataset'
batch_size = 10
epoch = 200
warmup_epoch = 10
learning_rate = 0.0002
gpu = "0"
log_dir = './exp_all_in'
npoint = 2048
ckpts = None

transforms = Compose([
    FixedPoints(npoint, replace=False, allow_duplicates=True),
])

num_classes = 2

train_path = pathlib.Path("/mnt/disk4/datasets/arheology/raw_taheometry_dataset/train")
valid_path = pathlib.Path("/mnt/disk4/datasets/arheology/raw_taheometry_dataset/train")
test_path = pathlib.Path("/mnt/disk4/datasets/arheology/raw_taheometry_dataset/test")
