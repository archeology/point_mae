from pathlib import Path
from typing import Optional
import numpy as np
import pandas as pd
from torch_geometric.data import Data
from torch_geometric.loader import DenseDataLoader
from torch_geometric.transforms import BaseTransform
import torch
import config


def create_dataset(dataset_path: Path, transforms: Optional[BaseTransform]):
    data = []
    for filepath in dataset_path.glob("*.csv"):
        df = pd.read_csv(filepath)
        x = np.stack(
            (
                df["x"],
                df["y"],
                df["h"],
            ),
            axis=-1,
        )
        
        y = np.array(df["objectType"])
        
        if config.num_classes == 3:
            y[y>2] = 0
        elif config.num_classes == 2:
            y[y>1] = 1
        else:
            raise ValueError("Dataloader supported for 3 or 2 classes")
        
        x = torch.from_numpy(x)
        y = torch.from_numpy(y)
        assert not torch.isnan(x).any()
        assert not torch.isnan(y).any()

        current_data = Data(pos=x, category=np.zeros([1, 1]), y=y, name=filepath.stem)
        current_data = transforms.forward(current_data)
        data.append(current_data)
    return data


train_loader = DenseDataLoader(
        create_dataset(config.train_path, config.transforms), batch_size=config.batch_size)

valid_loader = DenseDataLoader(        
        create_dataset(config.valid_path, config.transforms), batch_size=config.batch_size)

test_loader = DenseDataLoader(
    create_dataset(config.test_path, config.transforms), batch_size=1)