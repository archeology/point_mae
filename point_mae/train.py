# source https://github.com/Pang-Yatian/Point-MAE/blob/main/segmentation/main.py#L107

import datetime
import logging
import os
import sys
from pathlib import Path

import numpy as np
import torch
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from timm.scheduler import CosineLRScheduler
from tqdm import tqdm

import config
from point_mae.model import LossFunction, SegModel

from .dataset import train_loader, valid_loader

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = BASE_DIR
sys.path.append(os.path.join(ROOT_DIR, "models"))


if config.num_classes == 3:
    seg_classes = {"Archeology": [0, 1, 2]}
elif config.num_classes == 2:
    seg_classes = {"Archeology": [0, 1]}

seg_label_to_cat = {}  # {0:Archeology}
for cat in seg_classes.keys():
    for label in seg_classes[cat]:
        seg_label_to_cat[label] = cat


def inplace_relu(m):
    classname = m.__class__.__name__
    if classname.find("ReLU") != -1:
        m.inplace = True


def to_categorical(y, num_classes):
    """1-hot encodes a tensor"""
    new_y = torch.eye(num_classes)[y.cpu().data.numpy(),]
    if y.is_cuda:
        return new_y.cuda()
    return new_y


def start_train():
    def log_string(str):
        logger.info(str)
        print(str)

    """HYPER PARAMETER"""
    os.environ["CUDA_VISIBLE_DEVICES"] = config.gpu

    """CREATE DIR"""
    timestr = str(datetime.datetime.now().strftime("%Y-%m-%d_%H-%M"))
    exp_dir = Path("./log/")
    exp_dir.mkdir(exist_ok=True)
    exp_dir = exp_dir.joinpath("part_seg")
    exp_dir.mkdir(exist_ok=True)
    if config.log_dir is None:
        exp_dir = exp_dir.joinpath(timestr)
    else:
        exp_dir = exp_dir.joinpath(config.log_dir)
    exp_dir.mkdir(exist_ok=True)
    checkpoints_dir = exp_dir.joinpath("checkpoints/")
    checkpoints_dir.mkdir(exist_ok=True)
    log_dir = exp_dir.joinpath("logs/")
    log_dir.mkdir(exist_ok=True)

    """LOG"""
    logger = logging.getLogger("Model")
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    file_handler = logging.FileHandler("%s/%s.txt" % (log_dir, "point_mae"))
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    
    writer = SummaryWriter(log_dir=log_dir, comment='point_mae', filename_suffix='_seg')
    
    log_string("PARAMETER ...")
    log_string("point_mae")

    num_classes = config.num_classes
    num_part = config.num_classes

    """MODEL LOADING"""

    classifier = SegModel(num_part).cuda()
    criterion = LossFunction().cuda()
    classifier.apply(inplace_relu)
    print(
        "# generator parameters:",
        sum(param.numel() for param in classifier.parameters()),
    )
    start_epoch = 0

    if config.ckpts is not None:
        classifier.load_model_from_ckpt(config.ckpts)

    ## we use adamw and cosine scheduler
    def add_weight_decay(model, weight_decay=1e-5, skip_list=()):
        decay = []
        no_decay = []
        for name, param in model.named_parameters():
            if not param.requires_grad:
                continue  # frozen weights
            if (
                len(param.shape) == 1
                or name.endswith(".bias")
                or "token" in name
                or name in skip_list
            ):
                # print(name)
                no_decay.append(param)
            else:
                decay.append(param)
        return [
            {"params": no_decay, "weight_decay": 0.0},
            {"params": decay, "weight_decay": weight_decay},
        ]

    param_groups = add_weight_decay(classifier, weight_decay=0.05)
    optimizer = optim.AdamW(param_groups, lr=config.learning_rate, weight_decay=0.05)

    scheduler = CosineLRScheduler(
        optimizer,
        t_initial=config.epoch,
        lr_min=1e-6,
        warmup_lr_init=1e-6,
        warmup_t=config.warmup_epoch,
        cycle_limit=1,
        t_in_epochs=True,
    )

    best_acc = 0
    global_epoch = 0
    best_class_avg_iou = 0
    best_inctance_avg_iou = 0

    classifier.zero_grad()
    for epoch in range(start_epoch, config.epoch):
        mean_correct = []

        log_string("Epoch %d (%d/%s):" % (global_epoch + 1, epoch + 1, config.epoch))
        """Adjust learning rate and BN momentum"""

        classifier = classifier.train()
        loss_batch = []
        num_iter = 0
        """learning one epoch"""
        for i, data in tqdm(
            enumerate(train_loader), total=len(train_loader), smoothing=0.9
        ):
            num_iter += 1
            points, label, target = (
                data.pos.float().cuda(),
                data.category.long().cuda(),
                data.y.long().cuda(),
            )
            points = points.transpose(2, 1)

            seg_pred = classifier(points, to_categorical(label, num_classes))
            seg_pred = seg_pred.contiguous().view(-1, num_part)
            target = target.view(-1, 1)[:, 0]
            pred_choice = seg_pred.data.max(1)[1]

            correct = pred_choice.eq(target.data).cpu().sum()
            mean_correct.append(correct.item() / (config.batch_size * config.npoint))
            loss = criterion(seg_pred, target)
            loss.backward()
            optimizer.step()
            loss_batch.append(loss.detach().cpu())

            if num_iter == 1:
                torch.nn.utils.clip_grad_norm_(classifier.parameters(), 10, norm_type=2)
                num_iter = 0
                optimizer.step()
                classifier.zero_grad()

        if isinstance(scheduler, list):
            for item in scheduler:
                item.step(epoch)
        else:
            scheduler.step(epoch)

        train_instance_acc = np.mean(mean_correct)
        loss1 = np.mean(loss_batch)
        
        writer.add_scalar('accuracy/train', train_instance_acc, epoch)
        writer.add_scalar('loss/train', loss1, epoch)
        writer.add_scalar('lr/train', optimizer.param_groups[0]["lr"], epoch)

        with torch.no_grad():
            test_metrics = {}
            total_correct = 0
            total_seen = 0
            total_seen_class = [0 for _ in range(num_part)]
            total_correct_class = [0 for _ in range(num_part)]
            shape_ious = {cat: [] for cat in seg_classes.keys()}
            seg_label_to_cat = {}  # {0:Airplane, 1:Airplane, ...49:Table}

            for cat in seg_classes.keys():
                for label in seg_classes[cat]:
                    seg_label_to_cat[label] = cat

            classifier = classifier.eval()

            for batch_id, data in tqdm(
                enumerate(valid_loader), total=len(valid_loader), smoothing=0.9
            ):
                cur_batch_size, NUM_POINT, _ = data.pos.size()
                
                points, label, target = (
                    data.pos.float().cuda(),
                    data.category.long().cuda(),
                    data.y.long().cuda(),
                )
                
                points = points.transpose(2, 1)
                seg_pred = classifier(points, to_categorical(label, num_classes))
                cur_pred_val = seg_pred.cpu().data.numpy()
                cur_pred_val_logits = cur_pred_val
                cur_pred_val = np.zeros((cur_batch_size, NUM_POINT)).astype(np.int32)
                target = target.cpu().data.numpy()

                for i in range(cur_batch_size):
                    cat = seg_label_to_cat[target[i, 0]]
                    logits = cur_pred_val_logits[i, :, :]
                    cur_pred_val[i, :] = (
                        np.argmax(logits[:, seg_classes[cat]], 1) + seg_classes[cat][0]
                    )

                correct = np.sum(cur_pred_val == target)
                total_correct += correct
                total_seen += cur_batch_size * NUM_POINT

                for l in range(num_part):
                    total_seen_class[l] += np.sum(target == l)
                    total_correct_class[l] += np.sum(
                        (cur_pred_val == l) & (target == l)
                    )

                for i in range(cur_batch_size):
                    segp = cur_pred_val[i, :]
                    segl = target[i, :]
                    cat = seg_label_to_cat[segl[0]]
                    part_ious = [0.0 for _ in range(len(seg_classes[cat]))]
                    for l in seg_classes[cat]:
                        if (np.sum(segl == l) == 0) and (
                            np.sum(segp == l) == 0
                        ):  # part is not present, no prediction as well
                            part_ious[l - seg_classes[cat][0]] = 1.0
                        else:
                            part_ious[l - seg_classes[cat][0]] = np.sum(
                                (segl == l) & (segp == l)
                            ) / float(np.sum((segl == l) | (segp == l)))
                    shape_ious[cat].append(np.mean(part_ious))

            all_shape_ious = []
            for cat in shape_ious.keys():
                for iou in shape_ious[cat]:
                    all_shape_ious.append(iou)
                shape_ious[cat] = np.mean(shape_ious[cat])
            mean_shape_ious = np.mean(list(shape_ious.values()))
            test_metrics["accuracy"] = total_correct / float(total_seen)
            test_metrics["class_avg_accuracy"] = np.mean(
                np.array(total_correct_class)
                / np.array(total_seen_class, dtype=np.float64)
            )
            for cat in sorted(shape_ious.keys()):
                log_string(
                    "eval mIoU of %s %f"
                    % (cat + " " * (14 - len(cat)), shape_ious[cat])
                )
            test_metrics["class_avg_iou"] = mean_shape_ious
            test_metrics["inctance_avg_iou"] = np.mean(all_shape_ious)

        log_string(
            "Epoch %d test Accuracy: %f  Class avg mIOU: %f   Inctance avg mIOU: %f"
            % (
                epoch + 1,
                test_metrics["accuracy"],
                test_metrics["class_avg_iou"],
                test_metrics["inctance_avg_iou"],
            )
        )
                
        writer.add_scalar('accuracy/valid', test_metrics["accuracy"], epoch)
        writer.add_scalar('class_avg_iou/valid', test_metrics["class_avg_iou"], epoch)
        writer.add_scalar('inctance_avg_iou/tevalidst', test_metrics["inctance_avg_iou"], epoch)
        
        state = {
            "epoch": epoch,
            "train_acc": train_instance_acc,
            "test_acc": test_metrics["accuracy"],
            "class_avg_iou": test_metrics["class_avg_iou"],
            "inctance_avg_iou": test_metrics["inctance_avg_iou"],
            "model_state_dict": classifier.state_dict(),
            "optimizer_state_dict": optimizer.state_dict(),
        }
        
        logger.info("Save model...")
        savepath = str(checkpoints_dir) + f"/{epoch=} accuracy={test_metrics['accuracy']:.4f} iou={test_metrics['class_avg_iou']:.4f}.pth"
        log_string("Saving at %s" % savepath)
        torch.save(state, savepath)
        log_string("Saving model....")
        
        if test_metrics["inctance_avg_iou"] >= best_inctance_avg_iou:
            logger.info("Save best model...")
            savepath = str(checkpoints_dir) + "/best_model.pth"
            log_string("Saving at %s" % savepath)
            torch.save(state, savepath)
            log_string("Saving best model....")

        if test_metrics["accuracy"] > best_acc:
            best_acc = test_metrics["accuracy"]
        if test_metrics["class_avg_iou"] > best_class_avg_iou:
            best_class_avg_iou = test_metrics["class_avg_iou"]
        if test_metrics["inctance_avg_iou"] > best_inctance_avg_iou:
            best_inctance_avg_iou = test_metrics["inctance_avg_iou"]
        log_string("Best accuracy is: %.5f" % best_acc)
        log_string("Best class avg mIOU is: %.5f" % best_class_avg_iou)
        log_string("Best inctance avg mIOU is: %.5f" % best_inctance_avg_iou)
        global_epoch += 1


if __name__ == "__main__":
    start_train()
