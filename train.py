from point_mae.train import start_train


def run():
    start_train()


if __name__ == "__main__":
    run()
